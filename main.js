let config = {
  width: 800,
  height: 600,
  type: Phaser.AUTO,
  backgroundColor: 0x000000,
  scene: [BootGame, LoadScreen, MainMenu, PlayGame, HowToPlay]
}

let game = new Phaser.Game(config)
