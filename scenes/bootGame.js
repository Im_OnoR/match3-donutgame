class BootGame extends Phaser.Scene {
  constructor () {
    super('bootGame')
  }

  preload () {
    this.load.image('backgroundLoad', './images/backgrounds/007 Sunny Morning.png')
    this.load.image('logo', './images/donuts_logo.png')
    this.load.image('preloadBar', './images/phaser-game-progress-bar.png')
  }

  create () {
    this.scene.start('loadScreen')
  }
}
