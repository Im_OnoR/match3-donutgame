let gameMusic = {
	canPlay: false
}

class LoadScreen extends Phaser.Scene {
  constructor () {
    super('loadScreen')
  }

  preload () {
    this.add.image(0, 0, 'backgroundLoad').setOrigin(0)
    this.add.image(this.game.renderer.width / 2, 300, 'logo')
    this.preloadBar = this.add.image(0, 500, 'preloadBar').setOrigin(0)

    for (let i = 1; i <= 6; i++) {
      this.load.image('donut' + i, `./images/game/gem-0${i}.png`)
    }

    this.load.image('backgroundGame', './images/backgrounds/background.jpg')
    this.load.image('bgScore', './images/bg-score.png')
    this.load.image('bgTimer', './images/bg-timer.png')
    this.load.image('timeUp', './images/text-timeup.png')
    this.load.image('playButton', './images/btn-play.png')
    this.load.image('howToPlayButton', './images/btn-howToPlay.png')
    this.load.image('musicOnOption', './images/btn-sfx.png')
    this.load.image('musicOffOption', './images/btn-nosfx.png')
    this.load.image('hand', './images/game/hand.png')
    this.load.audio('menuMusic', './audio/background.mp3')


    this.load.once('progress', (percent) => {
      this.preloadBar.scaleX = percent + 2.78
    })
  }

  create () {
    this.input.setDefaultCursor('url(./images/game/NormalSelect.cur), pointer')
    gameMusic.music = this.sound.add('menuMusic', { loop: true })
    gameMusic.music.play()
    this.sound.pauseOnBlur = false
    this.scene.start('mainMenu')
  }
}
