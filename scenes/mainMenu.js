class MainMenu extends Phaser.Scene {
  constructor () {
    super('mainMenu')
  }
  create () {
    this.add.image(0, 0, 'backgroundLoad').setOrigin(0)
    this.add.image(this.game.renderer.width / 2, 100, 'logo').setScale(0.7)
   	// Button for play
    this.playButton = this.add.sprite(this.game.renderer.width / 2, this.game.renderer.height / 2 , 'playButton')
      .setScale(0.7)
      .setInteractive()
    this.playButton.on('pointerover', () => this.playButton.setScale(0.8))
    this.playButton.on('pointerout', () => this.playButton.setScale(0.7))
    this.playButton.on('pointerdown', () => {
      this.scene.start('playGame')
    })
    // Button for howToPlay
     this.howToPlayButton = this.add.sprite(this.game.renderer.width / 2, this.game.renderer.height / 2 + 100, 'howToPlayButton')
      .setScale(0.3)
      .setInteractive()
    this.howToPlayButton.on('pointerover', () => this.howToPlayButton.setScale(0.35))
    this.howToPlayButton.on('pointerout', () => this.howToPlayButton.setScale(0.3))
    this.howToPlayButton.on('pointerdown', () => {
      this.scene.start('howToPlay')
    })
    // Music
    
    musicOptions(this,  this.game.renderer.width / 2, this.game.renderer.height - 80)
  }
}

const musicOptions = (that , x , y) => {
	
  	gameMusic.musicOnButton = that.add.sprite(x, y, 'musicOnOption')
      .setScale(0.5)
      .setInteractive()
  	gameMusic.musicOffBotton = that.add.sprite( x, y, 'musicOffOption').setScale(0.5)

  
  	if(!gameMusic.canPlay){
  		gameMusic.musicOnButton.alpha = 1
  		gameMusic.musicOffBotton.visible = false
  	}

  gameMusic.musicOnButton.on('pointerdown', () => {
    if (gameMusic.canPlay) {
      gameMusic.music.resume()
      gameMusic.canPlay = !gameMusic.canPlay
      gameMusic.musicOnButton.alpha = 1
      gameMusic.musicOffBotton.visible = false
    } else {
      gameMusic.music.pause()
      gameMusic.canPlay = !gameMusic.canPlay
      gameMusic.musicOnButton.alpha = 0.01
      gameMusic.musicOffBotton.visible = true
    }
  })
}
