let gameOptions = {
  columns: 7,
  rows: 7,
  donutSize: 65,
  swapSpeed: 200,
  fallSpeed: 100,
  generateSpeed: 300,
  destroySpeed: 300,
  boardOffset: {
    x: 210,
    y: 50
  }
}

class PlayGame extends Phaser.Scene {
  constructor () {
    super('playGame'),
    this.timerMinutes = 1,
    this.timerSeconds = 0,
    this.canScore = false,
    this.score = 0,
    this.selectAccept = true,
    this.touchingCounter = 0,
    this.firstDonutData,
    this.secondDonutData,
    this.canDrag = false
  }

  create () {
  	// Initialize pointer
  	this.pointer = this.input.activePointer
  	// Bg image
    this.add.image(this.game.renderer.width / 2, 300, 'backgroundGame').setScale(0.65)
    // Timer
    this.timerImage = this.add.sprite(this.game.renderer.width + 5, this.game.renderer.height + 5, 'bgTimer')
      .setScale(0.8)
      .setOrigin(1, 1)
    this.timerText = this.add.text(this.game.renderer.width - 10, this.game.renderer.height - 10, ' ', { fontFamily: 'Fredoka One', fontSize: 48, fontFamily: 'cursive', color: '#ffffff' })
      .setOrigin(1, 1)
      .setShadow(1, 1, '#000000', 2)
    // Score
    this.scoreImage = this.add.sprite(-5, this.game.renderer.height + 5, 'bgScore')
      .setScale(0.8)
      .setOrigin(0, 1)
    this.scoreImage.depth = 10
    this.scoreText = this.add.text(10, this.game.renderer.height - 10, ' ', { fontFamily: 'Fredoka One', fontSize: 48, fontFamily: 'cursive', color: '#ffffff' })
      .setOrigin(0, 1)
      .setShadow(1, 1, '#000000', 2)
    this.scoreText.depth = 10
    // // BgMusic
    musicOptions(this , this.game.renderer.width / 2 , this.game.renderer.height - 50 )
    // MechanicsFunctions
    this.fillBoard()
    this.destroyDonuts()
    // TimeUp/GameOver
    this.timeUp = this.add.image(this.game.renderer.width / 2, this.game.renderer.height / 2 - 100, 'timeUp')
    this.timeUp.depth = 10
    this.timeUp.visible = false
    this.timerDelay = this.timerMinutes * 60000 + this.timerSeconds * 1000
    this.timer = this.time.addEvent({
		    delay: this.timerDelay,
		    callback: this.gameOver,
		    callbackScope: this
    })

    
  }

  update () {
    this.timerText.setText(this.formatTime(Math.round((this.timer.delay - this.timer.getElapsed()) / 1000)))
    this.scoreText.setText(this.score)
    if(this.canDrag){
    	this.dragSwap()
    }else{
    }
  }

  formatTime (s) {
    let minutes = Math.floor(s / 60)
    let seconds = '0' + (s - minutes * 60)
    return minutes + ':' + seconds.substr(-2)
  }

  gameOver () {
  	this.canDrag = false
    this.timeUp.visible = true
    this.selectAccept = false
    if(this.valid(this.firstDonutSelected)){
    	this.firstDonutSelected.setScale(0.7)
    }
    this.tweens.add({
	        targets: [this.timerText, this.timerImage, this.scoreText, this.scoreImage],
	        alpha: 0,
	        duration: gameOptions.destroySpeed,
	        callbackScope: this,
	        onComplete: () => {
	                	this.timerText.destroy()
	                	this.timerImage.destroy()
	                	this.scoreImage.setOrigin(0.5)
	                	this.scoreText.setOrigin(0.5, 0.3)
	                	this.tweens.add({ targets: [this.scoreImage, this.scoreText ],
									        x: this.game.renderer.width / 2,
									        y: this.game.renderer.height / 2 + 25,
									        alpha: 1,
									        duration: gameOptions.generateSpeed,
									        callbackScope: this,
									        onComplete: () => {
									        	this.scoreImage.alpha = 1
									        	this.scoreText.alpha = 1
									        }
									      })
	        }
	      })
  }

  fillBoard () {
    this.board = []
    for (let rows = 0; rows < gameOptions.rows; rows++) {
      this.board[rows] = []
      for (let columns = 0; columns < gameOptions.columns; columns++) {
        this.generateDonut(rows, columns)
      }
    }
  }

  generateDonut (rows, columns) {
    let value = Math.floor(Math.random() * 6) + 1
    let donut = this.addDonutSprite(rows, columns, value)
    donut.alpha = 0.1
    this.tweens.add({
      targets: donut,
      alpha: 1,
      duration: gameOptions.generateSpeed,
      callbackScope: this
    })
    this.board[rows][columns] = {
	    sprite: donut,
	    value: value
    }
  }

  addDonutSprite (rows, columns, value) {
    let x = gameOptions.boardOffset.x + rows * gameOptions.donutSize
    let y = gameOptions.boardOffset.y + columns * gameOptions.donutSize
    return this.add.sprite(x, y, 'donut' + value)
      .setOrigin(0.5)
      .setScale(0.7)
      .setInteractive()
      .on('pointerdown', function () { this.getDonutPoss() }.bind(this))
  }

  match () {
    let matchedCoordsArray = []
    for (let rows = 0; rows < gameOptions.rows; rows++) {
      for (let columns = 0; columns < gameOptions.columns; columns++) {
        if (this.isPartOfMatch(rows, columns)) {
          matchedCoordsArray.push({ rows, columns })
        }
      }
    }
    return matchedCoordsArray
  }

  destroyDonuts () {
    let matchedCoordsArray = this.match()
    let elementCount = 0
    if (matchedCoordsArray.length !== 0) {
	    for (let element of matchedCoordsArray) {
	    	if(this.canScore){
	    		this.score++	
	    	}
	      let x = element.rows
	      let y = element.columns
	      this.board[x][y].value = -1
	      this.tweens.add({
          targets: this.board[x][y].sprite,
          alpha: 0.1,
          scaleX: 0.1,
          scaleY: 0.1,
          duration: gameOptions.destroySpeed,
          callbackScope: this,
          onComplete: () => {
                    elementCount++
                    this.board[x][y].sprite.destroy()
                    if (elementCount == matchedCoordsArray.length) {
                      this.donutsFalls()
                    }
          }
        })
	    }
    } else {
    	this.canScore = true
    	this.canDrag = true
      this.getDonutPoss()
    }
  }

  donutsFalls () {
    for (let columns = 0; columns < gameOptions.columns; columns++) {
      outer: for (let rows = gameOptions.rows - 1; rows >= 0; rows--) {
        if (this.board[columns][rows].value < 0) {
          for (let higherRowElem = rows; higherRowElem >= 0; higherRowElem--) {
            if (this.board[columns][higherRowElem].value > 0) {
 							this.board[columns][rows].value = this.board[columns][higherRowElem].value
  						this.board[columns][higherRowElem].value = -1

              this.board[columns][rows].sprite = this.addDonutSprite(columns, rows, this.board[columns][rows].value)
              this.board[columns][higherRowElem].sprite.destroy()
              continue outer
            }
          }
        }
      }
    }

    this.reFillBoard()
  }

  reFillBoard () {
    for (let rows = 0; rows < gameOptions.rows; rows++) {
      for (let columns = 0; columns < gameOptions.columns; columns++) {
        if (this.board[columns][rows].value == -1) {
          this.generateDonut(columns, rows)
        }
      }
    }
    this.destroyDonuts()
  }

  getDonutPoss () {
  	if(this.selectAccept){
  		this.touchingCounter++
			if (!this.valid(this.firstDonutData) && this.touchingCounter > 1) {
				this.firstDonutData = this.selectDonut(this.pointer)
				this.firstDonutSelected = this.board[this.firstDonutData.coord_x][this.firstDonutData.coord_y].sprite.setScale(0.75)
  		} else if (!this.valid(this.secondDonutData) && this.valid(this.firstDonutData)) {
				this.secondDonutData = this.selectDonut(this.pointer)
				if (this.validSelect(this.firstDonutData.coord_x, this.firstDonutData.coord_y, this.secondDonutData.coord_x, this.secondDonutData.coord_y)) {
						this.swapDonuts(this.firstDonutData.coord_x, this.firstDonutData.coord_y, this.secondDonutData.coord_x, this.secondDonutData.coord_y)
				} else {
					this.touchingCounter = 1
					this.firstDonutSelected.setScale(0.7)
					delete this.firstDonutData
					delete this.secondDonutData
				}
  		}
  	}
  }

  dragSwap(){
  	if(this.pointer.isDown && this.pointer.justMoved){
  		let rows = Math.floor((this.pointer.x - gameOptions.boardOffset.x + gameOptions.donutSize / 2) / gameOptions.donutSize)
    	let columns = Math.floor((this.pointer.y - gameOptions.boardOffset.y + gameOptions.donutSize / 2) / gameOptions.donutSize)
    	if(this.valid(this.firstDonutData)){
	  		if(this.canSelect (rows, columns)){
	  				this.secondDonutData = this.selectDonut(this.pointer)
	  				if(this.validSelect(this.firstDonutData.coord_x, this.firstDonutData.coord_y, this.secondDonutData.coord_x, this.secondDonutData.coord_y)) {
						this.canDrag = false
						this.swapDonuts(this.firstDonutData.coord_x, this.firstDonutData.coord_y, this.secondDonutData.coord_x, this.secondDonutData.coord_y)
					}
	  		}
	  	}
    }
  }

  swapDonuts (columns1, rows1, columns2, rows2) {
  	this.replaceValues(columns1, rows1, columns2, rows2)
    if (this.isPartOfMatch(columns1, rows1) || this.isPartOfMatch(columns2, rows2)) {
      this.touchingCounter = 0
      this.swapDonutsTwenn (columns1, rows1, columns2, rows2)
    } else {
    	this.replaceValues(columns2, rows2, columns1, rows1)
      this.swapDonutsTwennBack(columns1, rows1, columns2, rows2)
    }
  }

  isPartOfMatch (rows, columns) {
    if (this.verticalMatch(rows, columns) || this.horisontalMatch(rows, columns)) {
      return true
    } else {
      return false
    }
  }

  verticalMatch (rows, columns, board = this.board) {
    if (this.valid(board[rows][columns]) && this.valid(board[rows][columns - 1]) && this.valid(board[rows][columns - 2]) &&
			board[rows][columns].value == board[rows][columns - 1].value && board[rows][columns].value == board[rows][columns - 2].value) {
      return true
    } else if (this.valid(board[rows][columns]) && this.valid(board[rows][columns + 1]) && this.valid(board[rows][columns + 2]) &&
			board[rows][columns].value == board[rows][columns + 1].value && board[rows][columns].value == board[rows][columns + 2].value) {
      return true
    } else if (this.valid(board[rows][columns]) && this.valid(board[rows][columns - 1]) && this.valid(board[rows][columns + 1]) &&
			board[rows][columns].value == board[rows][columns - 1].value && board[rows][columns].value == board[rows][columns + 1].value) {
      return true
    } else {
      return false
    }
  }

  horisontalMatch (rows, columns, board = this.board) {
    if (this.valid(board[rows]) && this.valid(board[rows - 1]) && this.valid(board[rows - 2]) &&
			board[rows][columns].value == board[rows - 1][columns].value && board[rows][columns].value == board[rows - 2][columns].value) {
      return true
    } else if (this.valid(board[rows]) && this.valid(board[rows + 1]) && this.valid(board[rows + 2]) &&
			board[rows][columns].value == board[rows + 1][columns].value && board[rows][columns].value == board[rows + 2][columns].value) {
      return true
    } else if (this.valid(board[rows]) && this.valid(board[rows - 1]) && this.valid(board[rows + 1]) &&
			board[rows][columns].value == board[rows - 1][columns].value && board[rows][columns].value == board[rows + 1][columns].value) {
      return true
    } else {
      return false
    }
  }

  valid (checkedValue) {
    if (checkedValue != undefined) {
      return true
    } else {
      return false
    }
  }

  selectDonut (pointer) {
  	let donutData = {}
    let coord_x = Math.floor((pointer.x - gameOptions.boardOffset.x + gameOptions.donutSize / 2) / gameOptions.donutSize)
    let coord_y = Math.floor((pointer.y - gameOptions.boardOffset.y + gameOptions.donutSize / 2) / gameOptions.donutSize)
    if(this.valid(this.board[coord_x][coord_y])){
    	donutData.value = this.board[coord_x][coord_y].value
    }
    donutData.coord_x = coord_x
    donutData.coord_y = coord_y
    return donutData
  }

  canSelect (rows, columns) {
    if (this.board[rows] != undefined && this.board[rows][columns] != undefined &&
			rows >= 0 && rows < gameOptions.rows && columns >= 0 && columns < gameOptions.columns) {
      return true
    } else {
      return false
    }
  }

  validSelect (x1, y1, x2, y2) {
  	if (y2 === y1 && x2 === x1 - 1 || y2 === y1 && x2 === x1 + 1 || x2 === x1 && y2 === y1 - 1 || x2 === x1 && y2 === y1 + 1) {
  		return true
  	} else {
  		return false
  	}
  }

  replaceValues (columns1, rows1, columns2, rows2) {
  	let tempValue
  	tempValue = this.board[columns1][rows1].value
    this.board[columns1][rows1].value = this.board[columns2][rows2].value
    this.board[columns2][rows2].value = tempValue
  }

  replaceSprites ( columns , rows){
  	this.board[columns][rows].sprite.destroy()
	  this.board[columns][rows].sprite = this.addDonutSprite(columns, rows, this.board[columns][rows].value)
  }

  swapDonutsTwenn (columns1, rows1, columns2, rows2){
  	this.firstDonutSwipe = this.tweens.add({
		  		targets: this.board[columns1][rows1].sprite,
			    x: gameOptions.boardOffset.x + columns2 * gameOptions.donutSize,
			    y: gameOptions.boardOffset.y + rows2 * gameOptions.donutSize,
			    ease: 'Linear',
			    duration: 200
      	})
  	this.secondDonutSwipe = this.tweens.add({
		  		targets: this.board[columns2][rows2].sprite,
			    x: gameOptions.boardOffset.x + columns1 * gameOptions.donutSize,
			    y: gameOptions.boardOffset.y + rows1 * gameOptions.donutSize,
			    ease: 'Linear',
			    duration: 200,
			    callbackScope: this,
	        onComplete: () => {
	                	this.replaceSprites(columns1, rows1)
    								this.replaceSprites(columns2, rows2)
    								delete this.firstDonutData
	    							delete this.secondDonutData
    								this.destroyDonuts()
	        }
      	})
  }

  swapDonutsTwennBack(columns1, rows1, columns2, rows2){
  	this.firstDonutSwipe = this.tweens.add({
		  		targets: this.board[columns1][rows1].sprite,
			    x: gameOptions.boardOffset.x + columns2 * gameOptions.donutSize,
			    y: gameOptions.boardOffset.y + rows2 * gameOptions.donutSize,
			    ease: 'Linear',
			    duration: 200,
			    yoyo: true
      	})
  	this.secondDonutSwipe = this.tweens.add({
		  		targets: this.board[columns2][rows2].sprite,
			    x: gameOptions.boardOffset.x + columns1 * gameOptions.donutSize,
			    y: gameOptions.boardOffset.y + rows1 * gameOptions.donutSize,
			    ease: 'Linear',
			    duration: 200,
			    yoyo: true,
			    callbackScope: this,
	        onComplete: () => {
	        					this.canDrag = true
	        					this.firstDonutSelected.setScale(0.7)
    								delete this.firstDonutData
	    							delete this.secondDonutData
      							this.touchingCounter = 0
	        					this.getDonutPoss()
	        }
      	})
  } 
}
